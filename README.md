# Portfolio_5320837

## Persönliche Daten
Name: Ersin Murat Zengin
E-Mail: ersin.murat.zengin@mnd.thm.de
Matrikelnummer: 5320837

## Beschreibung des Projekts
Das Ziel dieses Praktikums ist die Einrichtung einer voll funktionsfähigen Entwicklungsumgebung für Flutter sowie die
korrekte Einrichtung eines Git-Repositories, zu dem der Dozen eingeladen wird. Dies dient als Grundlage für die
zukünftige Arbeit und Bewertung im Rahmen des Kurses.

## Installation

Um dieses Projekt lokal auszuführen, folgen Sie bitte den nachstehenden Schritten:

1. Stellen Sie sicher, dass Sie Flutter auf Ihrem System installiert haben. Falls nicht, können Sie es von der offiziellen Flutter-Website herunterladen und installieren.

2. Klone das Repository von GitHub auf deinen lokalen Computer: bash git clone https://git.thm.de/emzn70/portfolio_5320837

3. Navigiere in das Verzeichnis des geklonten Projekts: cd Portfolio_5320837

4. Installiere die Abhängigkeiten des Projekts, indem du den Befehl flutter pub get ausführst.

5. Starte die Flutter-Anwendung, indem du den Befehl "flutter run" ausführst.

6. Öffne einen Webbrowser und navigiere zur angezeigten URL, um die lokale Instanz der Flutter-Anwendung anzuzeigen.

Nützliche Links Moodle Kurs: Der Kurs auf der Moodle-Plattform der THM, in dem das Projekt entwickelt wurde. Git Commit Conventions: Konventionen für Git-Commits, die während der Entwicklung des Projekts verwendet werden. Keyboard Shortcuts: Eine Liste von Tastenkombinationen für die Entwicklungsumgebung, die während der Projektentwicklung nützlich sein können.
