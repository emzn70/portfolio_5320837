import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Meine Homepage',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: HomePage(),
      routes: {
        '/about': (context) => AboutPage(),
        '/resume': (context) => ResumePage(),
        '/skills': (context) => SkillsPage(),
        '/projects': (context) => ProjectsPage(),
      },
    );
  }
}

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Startseite'),
      ),
      body: Center(
        child: Text(
          'Willkommen auf meiner Homepage!',
          style: TextStyle(fontSize: 24.0),
        ),
      ),
    );
  }
}

class AboutPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Über mich'),
      ),
      body: Center(
        child: Text(
          'Hier kannst du etwas über mich erfahren.',
          style: TextStyle(fontSize: 24.0),
        ),
      ),
    );
  }
}

class ResumePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Lebenslauf'),
      ),
      body: Center(
        child: Text(
          'Hier findest du meinen Lebenslauf.',
          style: TextStyle(fontSize: 24.0),
        ),
      ),
    );
  }
}

class SkillsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Fähigkeiten'),
      ),
      body: Center(
        child: Text(
          'Hier sind meine Fähigkeiten aufgelistet.',
          style: TextStyle(fontSize: 24.0),
        ),
      ),
    );
  }
}

class ProjectsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Projekte'),
      ),
      body: Center(
        child: Text(
          'Hier sind meine Projekte aufgelistet.',
          style: TextStyle(fontSize: 24.0),
        ),
      ),
    );
  }
}
